﻿namespace Eksperty.Consts
{
	public static class ClausesNames
	{
		public const string ProperForLongTrips = "properForLongTrips";
		public const string IsEcologic = "isEcologic";
		public const string UsageFrequency = "usageFrequency";
		public const string VehicleType = "vehicleType";
		public const string HasFourWheelsDrive = "hasFourWheelsDrive";
		public const string MaximumPrice = "maximumPrice";
		public const string ProperForOffRoad = "properForOffRoad";
		public const string ProperForCity = "properForCity";

		public const string SafetyImportance = "safetyImportance";
		public const string PerformanceImportance = "performanceImportance";
		public const string ReliabilityImportance = "reliabilityImportance";
	}
}
