﻿namespace Eksperty.Consts
{
	public enum VehicleType
	{
		small = 1,
		medium = 2,
		family_van = 3,
		suv = 4,
		delivery_car = 5,
		combi = 6
	}
}
