﻿namespace Eksperty.Consts
{
	public enum FuelType
	{
		diesel = 1,
		petrol = 2,
		electric = 3,
		hybrid = 4
	}
}
