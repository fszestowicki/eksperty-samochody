﻿namespace Eksperty.Consts
{
	public enum VehicleClass
	{
		economy = 1,
		compact = 2,
		intermediate = 3,
		full_size = 4,
		standard = 5,
		premium = 6,
		luxury = 7
	}
}
