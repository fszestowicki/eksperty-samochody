﻿namespace Eksperty.Consts
{
	public class Rules
	{
		public static readonly string StaticRules = $@"
		
			isEcologic(X) :- fuelType(X, electric).
			isEcologic(X) :- fuelType(X, hybrid).

			properForOffRoad(X) :- hasFourWheelsDrive(X).

			properForLongTrips(X) :- fuelType(X, diesel).
			properForLongTrips(X) :- fuelType(X, petrol).
			properForLongTrips(X) :- fuelType(X, hybrid).

			properForBusinessType(X) :- vehicleType(X, combi).
			properForBusinessType(X) :- vehicleType(X, medium).
			properForBusinessType(X) :- vehicleType(X, suv).

			properForBusinessClass(X) :- vehicleClass(X, standard).
			properForBusinessClass(X) :- vehicleClass(X, premium).
			properForBusinessClass(X) :- vehicleClass(X, luxury).
			properForBusinessTrips(X) :- properForBusinessType(X), properForBusinessClass(X).

			properForCityType(X) :- vehicleType(X, small).
			properForCityType(X) :- vehicleType(X, medium).
			properForCityFuelType(X) :- fuelType(X, Y), Y \= diesel.
			properForCity(X) :- properForCityFuelType(X), properForCityType(X).

			maxPrice(X, Y) :- price(X, Z), Y >= Z.
			requestedReliability(X, Y) :- reliability(X, Z), Z >= Y.
			requestedSafety(X, Y) :- safety(X, Z), Z >= Y.

			requestedPerformance(X, 0) :- car(X).
			requestedPerformance(X, 1) :- car(X).
			requestedPerformance(X, 2) :- acceleration(X, Y), Y < 12.0.
			requestedPerformance(X, 3) :- acceleration(X, Y), Y < 10.0.
			requestedPerformance(X, 4) :- acceleration(X, Y), Y < 8.0.
			requestedPerformance(X, 5) :- acceleration(X, Y), Y < 6.0.

			requestedEconomy(X, 0) :- car(X).
			requestedEconomy(X, 1) :- car(X).
			requestedEconomy(X, 2) :- requestedEconomy(X, 3).
			requestedEconomy(X, 2) :- fuelConsumption(X, Y), Y < 9.0.
			requestedEconomy(X, 3) :- requestedEconomy(X, 4).
			requestedEconomy(X, 3) :- fuelConsumption(X, Y), Y < 7.5.
			requestedEconomy(X, 4) :- requestedEconomy(X, 5).
			requestedEconomy(X, 4) :- fuelConsumption(X, Y), Y < 6.5.
			requestedEconomy(X, 5) :- fuelType(X, electric).
			requestedEconomy(X, 5) :- fuelConsumption(X, Y), Y < 5.0.

			properForWeddingClass(X) :- vehicleClass(X, premium).
			properForWeddingClass(X) :- vehicleClass(X, luxury).
			properForWedding(X) :- properForWeddingClass(X), passengersAmount(X, Y), Y > 2.";

		// Uzależnić performance od mcoy silnika, przyspieszenia itp.

		/* Reguły definiowane dla każdego pojazdu */
		/*
		    * car(X)
			
			* hasFourWheelsDrive(X).
			* fuelType(X, Y).
			* vehicleType(X, Y).

			* acceleration(X, Y).
			* reliability(X, Y).
			* safety(X, Y).
			* fuelConsumption(X, Y).
			
			* price(X, Y).
			* passengersAmount(X, Y).
			* vehicleClass(X, Y).
		*/
	}
}
