﻿namespace Eksperty.Consts
{
	public enum UsageFrequency
	{
		Everyday = 1,
		FewTimesPerWeek = 2,
		OnceAWeekOrLess = 3
	}
}
