﻿using Eksperty.Consts;
using Eksperty.Data;
using Eksperty.Models;
using Prolog;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Http;

namespace Eksperty.Controllers
{
	[RoutePrefix("questions")]
	public class QuestionsController : ApiController
	{
		[HttpGet, Route("")]
		public IEnumerable<Question> GetQuestions() => QuestionDataProvider.GetQuestions();

		[HttpPost, Route("results")]
		public IEnumerable<Car> GetResults(SearchQueryParams input)
		{
			var program = new PrologEngine();
			var cars = CarsDataProvider.GetCars();

			var rules = new List<string>();
			foreach (var c in cars)
				rules.AddRange(CreateCarRules(c));

			rules = rules.OrderBy(x => x).ToList();
			rules.Add(Rules.StaticRules);

			var filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "rules.pl");
			File.WriteAllLines(filePath, rules);

			program.Consult(filePath);
			var solutions = program.GetAllSolutions(filePath, CreateQuery(input));

			if (!solutions.Success)
				return new Car[0];

			var results = solutions.NextSolution.SelectMany(x => x.NextVariable.Select(v => v.Value));
			return cars.Where(x => results.Contains(x.Code));
		}
			
		private IEnumerable<string> CreateCarRules(Car car)
		{
			var dfi = new NumberFormatInfo() { NumberDecimalSeparator = "." };
			var result = new List<string>()
			{
				$"car({car.Code}).",
				$"vehicleType({car.Code}, {car.Type}).",
				$"fuelType({car.Code}, {car.FuelType}).",
				$"acceleration({car.Code}, {car.Acceleration.ToString("N", dfi)}).",
				$"fuelConsumption({car.Code}, {car.FuelConsumption.ToString("N", dfi)}).",
				$"safety({car.Code}, {car.Safety}).",
				$"reliability({car.Code}, {car.Reliability}).",
				$"price({car.Code}, {car.Price}).",
				$"passengersAmount({car.Code}, {car.MaxNumberOfPassengers}).",
				$"vehicleClass({car.Code}, {car.Class}).",
			};

			if (car.HasFourWheelsDrive)
				result.Add($"hasFourWheelsDrive({car.Code}).");

			return result;
		}

		private string CreateQuery(SearchQueryParams input)
		{
			var subqueries = new List<string>();

			if (input.Type.HasValue)
				subqueries.Add($"vehicleType(X, {input.Type.Value})");
			if (input.MaximumPrice.HasValue)
				subqueries.Add($"maxPrice(X, {input.MaximumPrice})");
			if (input.ReliabilityImportance.HasValue)
				subqueries.Add($"requestedReliability(X, {input.ReliabilityImportance})");
			if (input.PerformanceImportance.HasValue)
				subqueries.Add($"requestedPerformance(X, {input.PerformanceImportance})");
			if (input.SafetyImportance.HasValue)
				subqueries.Add($"requestedSafety(X, {input.SafetyImportance})");
			if (input.EconomyImportance.HasValue)
				subqueries.Add($"requestedEconomy(X, {input.EconomyImportance})");
			if (input.MountainRoadsUsage)
				subqueries.Add("properForOffRoad(X)");
			if (input.BusinessTripsUsage || input.HolidayTripsUsage)
				subqueries.Add("properForLongTrips(X)");
			if (input.BusinessTripsUsage)
				subqueries.Add("properForBusinessTrips(X)");
			if (input.CityUsage)
				subqueries.Add("properForCity(X)");
			if (input.WeddingCar)
				subqueries.Add("properForWedding(X)");
			if (input.Ecologic)
				subqueries.Add("isEcologic(X)");

			var subqueriesString = string.Join(", ", subqueries);
			return $"{subqueriesString}.";
		}
	}
}
