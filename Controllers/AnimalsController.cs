﻿using Eksperty.Models.Animals;
using Smile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Eksperty.Controllers
{
	[RoutePrefix("animals")]
	public class AnimalsController : ApiController
	{
		private const string DefaultSearchedNode = "Zwierze";

		[HttpPost, Route("")]
		public IEnumerable<GuessAnimalResult> GuessAnimal(GuessAnimalParams input)
		{
			try
			{
				input = input ?? new GuessAnimalParams();

				using (Network net = new Network())
				{
					net.ReadFile("siec_zwierzeta.xdsl");

					if (input != null)
						foreach (var node in input.NodesFilters)
							net.SetEvidence(node.Id, node.Value);

					net.UpdateBeliefs();
					var beliefs = net.GetNodeValue(input.SearchedNodeId ?? DefaultSearchedNode);

					return beliefs
						.Select((x, index) => new GuessAnimalResult()
						{
							Name = net.GetOutcomeId(input.SearchedNodeId ?? DefaultSearchedNode, index),
							Probability = x
						})
						.Where(x => x.Probability > 0)
						.OrderByDescending(x => x.Probability)
						.ToList();
				}
			}
			catch (Exception e)
			{
				return new GuessAnimalResult[0];
			}
		}
	}
}
