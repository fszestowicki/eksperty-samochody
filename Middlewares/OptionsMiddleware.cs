﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Eksperty.Middlewares
{
	class OptionsMiddleware : OwinMiddleware
	{
		public OptionsMiddleware(OwinMiddleware next) : base(next)
		{ }

		public override async Task Invoke(IOwinContext context)
		{
			if (context.Request.Method == HttpMethod.Options.Method)
			{
				context.Response.StatusCode = (int)HttpStatusCode.OK;
				return;
			}

			await Next.Invoke(context);
		}
	}
}
