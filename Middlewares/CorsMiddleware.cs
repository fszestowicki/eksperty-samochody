﻿using Microsoft.Owin;
using System.Threading.Tasks;

namespace Eksperty.Middlewares
{
	class CorsMiddleware : OwinMiddleware
	{
		public CorsMiddleware(OwinMiddleware next) : base(next)
		{ }

		public override async Task Invoke(IOwinContext context)
		{
			context.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "Origin", "Authorization", "X-Atmosphere-tracking-id", "X-Atmosphere-Framework", "X-Cache-Date", "Content-Type", "X-Atmosphere-Transport", "Accept-Language", "*" });
			context.Response.Headers.Add("Access-Control-Allow-Methods", new[] { "POST", "GET", "OPTIONS", "PUT", "DELETE" });
			context.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
			context.Response.Headers.Add("Access-Control-Request-Headers", new[] { "Origin", "X-Atmosphere-tracking-id", "X-Atmosphere-Framework", "X-Cache-Date", "Content-Type", "X-Atmosphere-Transport", "Accept-Language", "*" });
			context.Response.Headers.Add("Accept-Language", new[] { "pl-PL", "en-GB" });

			await Next.Invoke(context);
		}
	}
}
