﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eksperty.Middlewares
{
	class ExceptionHandlingMiddleware : OwinMiddleware
	{
		private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
		public ExceptionHandlingMiddleware(OwinMiddleware next) : base(next) { }

		public override async Task Invoke(IOwinContext context)
		{
			try
			{
				await Next.Invoke(context);
			}
			catch (ObjectDisposedException) { /* Anulowanie requesta przez klienta */ }
			catch (Exception ex)
			{
				_logger.Error(ex);
				context.Response.Write(ex.Message);
			}
		}
	}
}
