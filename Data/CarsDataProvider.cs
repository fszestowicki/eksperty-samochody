﻿using Eksperty.Models;
using System.Collections.Generic;

namespace Eksperty.Data
{
	class CarsDataProvider
	{
		public static IEnumerable<Car> GetCars()
		{
			return new Car[]
			{
				new Car()
				{
					Code = "zafira",
					Name = "Opel Zafira",
					FuelType = Consts.FuelType.diesel,
					HasFourWheelsDrive = false,
					Acceleration = 11.2F,
					FuelConsumption = 7.1F,
					Price = 150000,
					Reliability = 4,
					Safety = 4,
					Class = Consts.VehicleClass.standard,
					MaxNumberOfPassengers = 7,
					Type = Consts.VehicleType.family_van
				},
				new Car()
				{
					Code = "sprinter",
					Name = "Mercedes Sprinter",
					FuelType = Consts.FuelType.diesel,
					HasFourWheelsDrive = true,
					Acceleration = 16.3F,
					FuelConsumption = 12.7F,
					Price = 122000,
					Reliability = 4,
					Safety = 5,
					Class = Consts.VehicleClass.full_size,
					MaxNumberOfPassengers = 3,
					Type = Consts.VehicleType.delivery_car
				},
				new Car()
				{
					Code = "rio",
					Name = "Kia Rio",
					FuelType = Consts.FuelType.petrol,
					HasFourWheelsDrive = false,
					Acceleration = 11.5F,
					FuelConsumption = 6.7F,
					Price = 49000,
					Reliability = 5,
					Safety = 4,
					Class = Consts.VehicleClass.compact,
					MaxNumberOfPassengers = 5,
					Type = Consts.VehicleType.small
				},
				new Car()
				{
					Code = "jaguar_xj",
					Name = "Jaguar XJ",
					FuelType = Consts.FuelType.diesel,
					HasFourWheelsDrive = false,
					Acceleration = 5.9F,
					FuelConsumption = 9.4F,
					Price = 490000,
					Reliability = 5,
					Safety = 4,
					Class = Consts.VehicleClass.luxury,
					MaxNumberOfPassengers = 5,
					Type = Consts.VehicleType.medium
				},
				new Car()
				{
					Code = "passat",
					Name = "Volkswagen Passat",
					FuelType = Consts.FuelType.diesel,
					HasFourWheelsDrive = false,
					Acceleration = 7.7F,
					FuelConsumption = 6.7F,
					Price = 100000,
					Reliability = 3,
					Safety = 5,
					Class = Consts.VehicleClass.standard,
					MaxNumberOfPassengers = 5,
					Type = Consts.VehicleType.combi
				},
				new Car()
				{
					Code = "volvo_xc60",
					Name = "Volwo XC60",
					FuelType = Consts.FuelType.diesel,
					HasFourWheelsDrive = true,
					Acceleration = 8.3F,
					FuelConsumption = 7.9F,
					Price = 170000,
					Reliability = 5,
					Safety = 5,
					Class = Consts.VehicleClass.premium,
					MaxNumberOfPassengers = 5,
					Type = Consts.VehicleType.suv
				},
				new Car()
				{
					Code = "ioniq",
					Name = "Hyundai Ioniq",
					FuelType = Consts.FuelType.electric,
					HasFourWheelsDrive = false,
					Acceleration = 9.7F,
					FuelConsumption = 0,
					Price = 178000,
					Reliability = 5,
					Safety = 4,
					Class = Consts.VehicleClass.compact,
					MaxNumberOfPassengers = 5,
					Type = Consts.VehicleType.medium
				},
				new Car()
				{
					Code = "forfour",
					Name = "Smart Forfour",
					FuelType = Consts.FuelType.electric,
					HasFourWheelsDrive = false,
					Acceleration = 12.7F,
					FuelConsumption = 0,
					Price = 96000,
					Reliability = 2,
					Safety = 3,
					Class = Consts.VehicleClass.economy,
					MaxNumberOfPassengers = 4,
					Type = Consts.VehicleType.small
				},
				new Car()
				{
					Code = "jimmy",
					Name = "Suzuki Jimmy",
					FuelType = Consts.FuelType.petrol,
					HasFourWheelsDrive = true,
					Acceleration = 12.0F,
					FuelConsumption = 7.3F,
					Price = 96000,
					Reliability = 5,
					Safety = 2,
					Class = Consts.VehicleClass.compact,
					MaxNumberOfPassengers = 4,
					Type = Consts.VehicleType.medium
				},
				new Car()
				{
					Code = "yaris_hybrid",
					Name = "Toyota Yaris Hybrid",
					FuelType = Consts.FuelType.hybrid,
					HasFourWheelsDrive = false,
					Acceleration = 11.8F,
					FuelConsumption = 4.9F,
					Price = 65000,
					Reliability = 5,
					Safety = 3,
					Class = Consts.VehicleClass.economy,
					MaxNumberOfPassengers = 5,
					Type = Consts.VehicleType.small
				},
				new Car()
				{
					Code = "tesla_model_s",
					Name = "Tesla Model S",
					FuelType = Consts.FuelType.electric,
					HasFourWheelsDrive = true,
					Acceleration = 2.6F,
					FuelConsumption = 0,
					Price = 363000,
					Reliability = 1,
					Safety = 5,
					Class = Consts.VehicleClass.luxury,
					MaxNumberOfPassengers = 5,
					Type = Consts.VehicleType.medium
				}
			};
		}
	}
}
