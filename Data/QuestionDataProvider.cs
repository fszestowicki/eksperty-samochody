﻿using Eksperty.Consts;
using Eksperty.Models;
using System.Collections.Generic;

namespace Eksperty.Data
{
	public class QuestionDataProvider
	{
		public static IEnumerable<Question> GetQuestions()
		{
			return new[]
			{
				new Question()
				{
					Text = "Do jakich celów zamierzasz użytkować pojazd?",
					AllowMultipleAnswers = true,
					Answers = new[]
					{
						new Answer("Dojazdy do pracy i codzienne sprawy"),
						new Answer("Wyjazdy na wakacje"),
						new Answer("Wyjazdy biznesowe"),
					}
				},
				new Question()
				{
					Text = "Jak często korzystasz z pojazdu?",
					AllowMultipleAnswers = false,
					Answers = new[]
					{
						new Answer("Codziennie"),
						new Answer("Kilka razy w tygodniu"),
						new Answer("Raz w tygodniu lub rzadziej"),
					}
				},
				new Question()
				{
					Text = "Jakiego pojazdu poszukujesz?",
					AllowMultipleAnswers = false,
					Answers = new[]
					{
						new Answer("Małe"),
						new Answer("Średnie"),
						new Answer("Auto rodzinne"),
						new Answer("SUV"),
						new Answer("Dostawczak"),
					}
				},
				new Question()
				{
					Text = "Do jakiej ceny poszukujesz auta?",
					AllowMultipleAnswers = false,
					Answers = new Answer[0],
				},
				new Question()
				{
					Text = "Czy zdarza Ci się jeździć po górskich lub nieasfaltowanych drogach?",
					AllowMultipleAnswers = false,
					Answers = new[]
					{
						new Answer("Tak"),
						new Answer("Nie")
					}
				},
				new Question()
				{
					Text = "Czy jeździsz autem po mieście?",
					AllowMultipleAnswers = false,
					Answers = new[]
					{
						new Answer("Tak"),
						new Answer("Nie")
					}
				},
				new Question()
				{
					Text = "Jak ważne jest dla Ciebie bezpieczeństwo?",
					AllowMultipleAnswers = false,
					Answers = new[]
					{
						new Answer("1"),
						new Answer("2"),
						new Answer("3"),
						new Answer("4"),
						new Answer("5")
					}
				},
				new Question()
				{
					Text = "Jak ważne są dla Ciebie osiągi?",
					AllowMultipleAnswers = false,
					Answers = new[]
					{
						new Answer("1"),
						new Answer("2"),
						new Answer("3"),
						new Answer("4"),
						new Answer("5")
					}
				},
				new Question()
				{
					Text = "Jak ważna jest dla Ciebie bezwarayjność?",
					AllowMultipleAnswers = false,
					Answers = new[]
					{
						new Answer("1"),
						new Answer("2"),
						new Answer("3"),
						new Answer("4"),
						new Answer("5")
					}
				},
				new Question()
				{
					Text = "Jak ważne są dla Ciebie koszty eksploatacji?",
					AllowMultipleAnswers = false,
					Answers = new[]
					{
						new Answer("1"),
						new Answer("2"),
						new Answer("3"),
						new Answer("4"),
						new Answer("5")
					}
				},
				new Question()
				{
					Text = "Czy ważna jest dla Ciebie ekologiczność",
					AllowMultipleAnswers = false,
					Answers = new[]
					{
						new Answer("Tak"),
						new Answer("Nie")
					}
				},
				new Question()
				{
					Text = "Czy poszukujesz auta na ślub?",
					AllowMultipleAnswers = false,
					Answers = new[]
					{
						new Answer("Tak"),
						new Answer("Nie")
					}
				},
			};
		}
	}
}
