﻿using Microsoft.Owin.Hosting;
using Smile;
using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Eksperty
{
	class Program
	{
		static void Main(string[] args)
		{
			InitializeSmile();

			Task.Run(() =>
			{
				var options = new StartOptions();
				options.Urls.Add("http://*:8150");
				var webApp = WebApp.Start(options, new Startup().Configuration);
			});

			while (true)
				Thread.Sleep(TimeSpan.FromHours(1));
		}

		static void InitializeSmile()
		{
			new Smile.License(
				"SMILE LICENSE 931ec234 ce32876a 0b07d8d8 " +
				"THIS IS AN ACADEMIC LICENSE AND CAN BE USED " +
				"SOLELY FOR ACADEMIC RESEARCH AND TEACHING, " +
				"AS DEFINED IN THE BAYESFUSION ACADEMIC " +
				"SOFTWARE LICENSING AGREEMENT. " +
				"Serial #: 3d1uutezx96aw9aneyik4bxid " +
				"Issued for: Filip Szestowicki (filipszestowicki@gmail.com) " +
				"Academic institution: AGH University of Science and Technology in Cracow " +
				"Valid until: 2020-07-15 " +
				"Issued by BayesFusion activation server",
				new byte[] {
					0xa6,0x74,0xa9,0x0e,0x7b,0xb7,0x3b,0xd9,0xc5,0xa0,0xd9,0xa8,0x21,0x73,0x07,0x9f,
					0x51,0x6d,0x36,0x27,0x43,0x53,0xd0,0xc3,0xde,0x89,0xca,0x49,0xa2,0x3f,0xdb,0xd2,
					0x31,0xf8,0x0c,0xa6,0x9b,0x35,0xe1,0x98,0xd6,0xd4,0x2e,0x91,0xd2,0x8f,0xd1,0x8e,
					0x11,0x8b,0xf2,0xa6,0xb9,0xfe,0x85,0xc0,0xc6,0xdf,0x34,0x6a,0x97,0xff,0xe8,0x81
				});
		}
	}
}
