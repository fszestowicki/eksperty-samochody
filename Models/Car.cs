﻿using Eksperty.Consts;

namespace Eksperty.Models
{
	public class Car
	{
		public string Code { get; set; }
		public string Name { get; set; }

		public FuelType FuelType { get; set; }
		public bool HasFourWheelsDrive { get; set; }
		public VehicleType Type { get; set; }
		public VehicleClass Class { get; set; }

		public int Price { get; set; }
		public int MaxNumberOfPassengers { get; set; }

		public float Acceleration { get; set; }
		public float FuelConsumption { get; set; }
		public int Reliability { get; set; }
		public int Safety { get; set; }
	}
}
