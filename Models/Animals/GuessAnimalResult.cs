﻿namespace Eksperty.Models.Animals
{
	public class GuessAnimalResult
	{
		public string Name { get; set; }
		public double Probability { get; set; }
	}
}
