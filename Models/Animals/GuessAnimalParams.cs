﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eksperty.Models.Animals
{
	public class GuessAnimalParams
	{
		public GuessAnimalParams()
		{
			NodesFilters = new GuessAnimalNodeParams[0];
		}

		/// <summary> Id węzła, którego prawdopodobieństwo ma zostać wyliczone (np. Zwierze)</summary>
		public string SearchedNodeId { get; set; }

		/// <summary> Parametry filtrowania zwierząt</summary>
		public IEnumerable<GuessAnimalNodeParams> NodesFilters { get; set; }
	}

	public class GuessAnimalNodeParams
	{
		/// <summary> Id węzła, po którym ma zostać wykonane filtrowanie (np. Kolor) </summary>
		public string Id { get; set; }
		/// <summary> Wartość ustawiona w węźle (np. Brazowy)</summary>
		public string Value { get; set; }
	}
}
