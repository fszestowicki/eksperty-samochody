﻿using Eksperty.Consts;
using System.Collections.Generic;

namespace Eksperty.Models
{
	public class Question
	{
		public string Text { get; set; }

		public bool AllowMultipleAnswers { get; set; }

		public IEnumerable<Answer> Answers { get; set; }
	}
}
