﻿using Eksperty.Consts;

namespace Eksperty.Models
{
	public class SearchQueryParams
	{
		public bool CityUsage { get; set; }

		public bool EverydayUsage { get; set; }
		public bool HolidayTripsUsage { get; set; }
		public bool BusinessTripsUsage { get; set; }
		public bool MountainRoadsUsage { get; set; }

		/// <summary> Liczba z zakresu 1-5 </summary>
		public int? SafetyImportance { get; set; }
		/// <summary> Liczba z zakresu 1-5 </summary>
		public int? PerformanceImportance { get; set; }
		/// <summary> Liczba z zakresu 1-5 </summary>
		public int? ReliabilityImportance { get; set; }
		/// <summary> Liczba z zakresu 1-5 </summary>
		public int? EconomyImportance { get; set; }
		public int? MaximumPrice { get; set; }

		/// <summary>
		/// Typ pojazdu. Dostępne typy:
		/// 1 - Mały
		/// 2 - Średni
		/// 3 - VAN rodzinny
		/// 4 - SUV
		/// 5 - Dostawczak
		/// </summary>
		public VehicleType? Type { get; set; }
		/// <summary>
		/// Częstotliwość użytkowania
		/// 1 - Codziennie
		/// 2 - Kilka razy w tygodniu
		/// 3 - Raz w tygodniu lub rzadziej
		/// </summary>
		public UsageFrequency? UsageFrequency { get; set; }

		public bool WeddingCar { get; set; }
		public bool Ecologic { get; set; }
	}
}
