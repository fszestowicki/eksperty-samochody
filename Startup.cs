﻿using Eksperty.Middlewares;
using Newtonsoft.Json;
using NSwag.AspNet.Owin;
using Owin;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace Eksperty
{
	public class Startup
	{
		protected static System.Net.HttpListener GetListener(IAppBuilder app)
		{
			return (System.Net.HttpListener)app.Properties["System.Net.HttpListener"];
		}

		protected HttpConfiguration ConfigureWebApi()
		{
			var config = new HttpConfiguration();
			config.MapHttpAttributeRoutes();

			var json = config.Formatters.JsonFormatter;
			json.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
			json.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
			json.SerializerSettings.DateFormatString = "yyyy-MM-ddTHH:mm:ss.FFFFFFFzzz";

			config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
			config.Services.Replace(typeof(IExceptionHandler), new PassthroughExceptionHandler());

			return config;
		}

		public void Configuration(IAppBuilder app)
		{
			var webApiConfiguration = ConfigureWebApi();

			app.Use<ExceptionHandlingMiddleware>();
			app.Use<CorsMiddleware>();
			app.Use<OptionsMiddleware>();

			app.UseWebApi(webApiConfiguration);

			app.UseSwagger(Assembly.GetExecutingAssembly(), config => { });

			app.UseSwaggerUi3();
		}

		public class PassthroughExceptionHandler : IExceptionHandler
		{
			public Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
			{
				var info = ExceptionDispatchInfo.Capture(context.Exception);
				info.Throw();

				return Task.FromResult(0);
			}
		}
	}
}
